import React, { useContext } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

//Login
import loginScreen from './screens/LoginScreen';
import registerScreen from './screens/RegisterScreen';

//Carrito
import cartScreen from './screens/CartScreen';
import listScreen from './screens/ListScreen';

const Stack = createNativeStackNavigator();

function MyStack() {
    return (
        <Stack.Navigator
            initialRouteName="LoginScreen"
        >
            <Stack.Screen
                name="LoginScreen"
                component={loginScreen}
            />
            <Stack.Screen
                name="RegisterScreen"
                component={registerScreen}
                options={{
                    headerBackTitleVisible: false,
                }}
            />

            <Stack.Screen
                name="ListScreen" component={listScreen}
            />
            <Stack.Screen
                name="Cart" component={cartScreen} options={{ headerBackTitleVisible: false, }}
            />


        </Stack.Navigator>
    );
}

export default function Navigation() {
    return (
        <NavigationContainer>
            <MyStack />
        </NavigationContainer>
    );
}