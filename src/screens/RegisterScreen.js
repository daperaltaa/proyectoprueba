import React, { useState } from 'react';
import { Button, Text, TextInput, TouchableOpacity, View, StyleSheet, Alert } from 'react-native';
import { useNavigation } from "@react-navigation/native";

const RegisterScreen = () => {

    const navigation = useNavigation();
    const [username, setUsername] = useState(null);
    const [email, setEmail] = useState(null);
    const [password1, setPassword1] = useState(null);
    const [password2, setPassword2] = useState(null);
    const [FirstName, setFirstName] = useState(null);
    const [LastName, setLastName] = useState(null);
    const group = "Suscriptor"


    const register = (username, email, password1, password2, FirstName, LastName) => {
        fetch('http://78.46.69.39:8007/rest-auth/registration/', {
            credentials: 'omit',
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                'username': username,
                'email': email,
                'password1': password1,
                'password2': password2,
                'first_name': FirstName,
                'last_name': LastName,
                'group': group
            })
        })
            .then((res) => {
                if (res.status == 200 || res.status == 201) {
                    navigation.navigate("LoginScreen")
                    alert("Registro Exitoso")
                } else {
                    return res.json();
                }
            })
            .then(res => {
                if (res) {
                    alert(JSON.stringify(res))
                }
            })
            .catch(e => {
                alert(e)
            });
    }


    return (
        <View style={styles.container}>
            <View style={styles.wrapper}>

                <Text style={styles.titulo}>Registro</Text>
                <Text>Username</Text>
                <TextInput
                    style={styles.input}
                    value={username}
                    placeholder="Enter Username"
                    onChangeText={text => setUsername(text)}
                />

                <Text>Email</Text>
                <TextInput
                    style={styles.input}
                    value={email}
                    type="email"
                    autoCapitalize="none"
                    placeholder="Enter email"
                    onChangeText={text => setEmail(text)}
                />
                <Text>Password</Text>
                <TextInput
                    style={styles.input}
                    value={password1}
                    placeholder="Enter password"
                    onChangeText={text => setPassword1(text)}
                    secureTextEntry
                />
                <Text>Password Again</Text>
                <TextInput
                    style={styles.input}
                    value={password2}
                    placeholder="Enter password Again"
                    onChangeText={text => setPassword2(text)}
                    secureTextEntry
                />
                <Text>First Name</Text>
                <TextInput
                    style={styles.input}
                    value={FirstName}
                    placeholder="Enter name"
                    onChangeText={text => setFirstName(text)}
                />
                <Text>Last Name</Text>
                <TextInput
                    style={styles.input}
                    value={LastName}
                    placeholder="Enter Last Name"
                    onChangeText={text => setLastName(text)}
                />

                <Button
                    title="Register"
                    onPress={() => {
                        register(username, email, password1, password2, FirstName, LastName);
                    }}
                />

                <View style={{ flexDirection: 'row', marginTop: 20 }}>
                    <Text>¿Ya tienes una cuenta? </Text>
                    <TouchableOpacity onPress={() => navigation.navigate('LoginScreen')}>
                        <Text style={styles.link}>Login</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        paddingTop: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
    wrapper: {
        width: '80%',
    },
    input: {
        marginBottom: 10,
        borderWidth: 1,
        borderColor: '#bbb',
        borderRadius: 5,
        paddingHorizontal: 14,
    },
    link: {
        color: 'blue',
    },
    titulo: {
        fontSize: 30,
        textAlign: 'center',
        color: "#000000",
        paddingBottom: 30,
    },
});

export default RegisterScreen;
