import React, { useEffect, useState } from "react";
import { View, Text, StyleSheet, FlatList, TouchableOpacity, Image, ActivityIndicator, RefreshControl } from "react-native";
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/Ionicons';
import { useNavigation } from "@react-navigation/native";

let productos = []
let ids = []

const CartScreen = () => {

  const navigation = useNavigation();
  const [isLoading, setLoading] = useState(true);
  const [refreshing, setRefreshing] = useState(false);
  const [Total, setTotal] = useState(0);

  Eliminar = (item) => {
    let id = item.producto.id;
    for (let index = 0; index < productos.length; index++) {
      if (productos[index].producto.id == id) {
        productos.splice(index, 1);
      }
    }
    for (let a = 0; a < ids.length; a++) {
      if (ids[a] == id) {
        ids.splice(a, 1);
      }
    }
    TotalPagar();
  }

  Aumentar = (id) => {
    for (let index = 0; index < productos.length; index++) {
      if (productos[index].producto.id == id) {
        productos[index].cantidad = productos[index].cantidad + 1;
      }
    }
    TotalPagar();
  }

  Disminuir = (id) => {
    for (let index = 0; index < productos.length; index++) {
      if (productos[index].producto.id == id) {
        if (productos[index].cantidad > 1) {
          productos[index].cantidad = productos[index].cantidad - 1;
        }
      }
    }
    TotalPagar();
  }

  TotalPagar = () => {
    let total = 0;
    for (let i = 0; i < productos.length; i++) {
      let productPrice = productos[i].producto.total * productos[i].cantidad;
      total = total + productPrice;
    }
    setTotal(total)
  }

  Pagar = async () => {
    if (productos.length == 0) {
      alert("El carrito no tiene productos");

    } else {
      productos.splice(0, productos.length);
      ids.splice(0, ids.length);
      AsyncStorage.clear();
      alert("Compra realizada")
      navigation.navigate("ListScreen")
      TotalPagar();
    }
  }


  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('CartProducts')
      const obj = JSON.parse(jsonValue);
      if (obj) {
        if (!ids.includes(obj.producto.id)) {
          productos.push(obj)
          ids.push(obj.producto.id);
        }
      }

      setLoading(false);
      await AsyncStorage.removeItem('CartProducts');
      TotalPagar();
    } catch (e) {
      console.log(e)
    }
  }

  const onRefresh = React.useCallback(async () => {
    setRefreshing(true)
    await getData();
    setRefreshing(false)
  })

  useEffect(() => {
    getData();
  }, []);

  return (

    <View style={styles.container}>

      {isLoading ? <ActivityIndicator /> : (
        <FlatList
          style={styles.userList}
          data={productos}
          keyExtractor={item => item.producto.id}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={onRefresh}
            />
          }
          renderItem={({ item }) => {
            return (
              <TouchableOpacity style={styles.card}>
                <Image style={styles.image} source={require('../assets/producto.png')} />
                <View style={styles.cardContent}>
                  <Text style={styles.name} >{item.producto.referencia.nombre}</Text>
                  <Text  >{item.descripcion}</Text>
                  <Text style={styles.price}>$ {item.producto.total * item.cantidad}</Text>
                  <TouchableOpacity style={styles.followButton} onPress={() => this.Eliminar(item)}>
                    <Text style={styles.followButtonText}>Eliminar</Text>
                  </TouchableOpacity>

                  <View style={{ flexDirection: 'row', paddingTop: 10 }}>

                    <TouchableOpacity onPress={() => this.Disminuir(item.producto.id)}>
                      <Icon name="remove-circle-outline" size={30} color={"#33c37d"} />
                    </TouchableOpacity>

                    <Text style={styles.cantidad} >{item.cantidad}</Text>

                    <TouchableOpacity onPress={() => this.Aumentar(item.producto.id)}>
                      <Icon name="ios-add-circle-outline" size={30} color={"#33c37d"} />
                    </TouchableOpacity>

                  </View>
                </View>
              </TouchableOpacity>
            )
          }}
        />
      )}

      <Text style={{ fontWeight: 'bold', color: "#000000", fontSize: 25, textAlign: "center" }}  >Total Pagar: ${Total}</Text>
      <View style={{ height: 20 }} />

      <TouchableOpacity style={styles.botonpay} onPress={() => this.Pagar()}>
        <Text style={styles.Textbotonpay}>Pagar</Text>
      </TouchableOpacity>

      <View style={{ height: 20 }} />
    </View>

  );

};


export default CartScreen;


const styles = StyleSheet.create({

  image: {
    width: 100,
    height: 100,
  },
  container: {
    flex: 1,
    marginTop: 20,
    backgroundColor: "#eeeeee",
  },
  userList: {
    flex: 1,
  },
  cardContent: {
    marginLeft: 20,
    marginTop: 10
  },
  card: {
    shadowColor: '#00000021',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,

    marginVertical: 10,
    marginHorizontal: 20,
    backgroundColor: "white",
    flexBasis: '46%',
    padding: 10,
    flexDirection: 'row',
    borderRadius: 30,
  },
  followButton: {
    marginTop: 10,
    height: 35,
    width: 100,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    backgroundColor: "#f44336",
  },
  followButtonText: {
    color: "#FFFFFF",
    fontSize: 20,
  },
  name: {
    fontSize: 17,
    flex: 1,
    color: "#008080",
    fontWeight: 'bold'
  },
  price: {
    fontSize: 20,
    flex: 1,
    color: "#2b2b2b",
    fontWeight: 'bold'
  },
  botonpay: {
    backgroundColor: "#00BFFF",
    alignItems: 'center',
    padding: 10,
    borderRadius: 20,
    margin: 20

  },
  Textbotonpay: {
    fontSize: 24,
    fontWeight: "bold",
    color: 'white',
  },
  cantidad: {
    fontWeight: 'bold',
    color: "#000000",
    fontSize: 20,
    paddingRight: 20,
    paddingLeft: 20
  }


});
