import React, { useEffect, useState } from "react";
import { View, Text, StyleSheet, FlatList, TouchableOpacity, Image, ActivityIndicator } from "react-native";
import { useNavigation } from "@react-navigation/native";
import AsyncStorage from '@react-native-async-storage/async-storage';

export default ListScreen = () => {
    const navigation = useNavigation();
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState([]);

    const getProductos = async () => {
        try {
            const response = await fetch("http://78.46.69.39:8007/api-referencias/referenciassearch/");
            const json = await response.json();
            setData(json);

        } catch (error) {
            alert(error);
        } finally {
            setLoading(false);
        }
    }

    addProduct = (item) => {
        navigation.navigate("Cart")
        const itemcar = {
            producto: item,
            cantidad: 1
        }
        storeData(itemcar);
    }

    const storeData = async (value) => {
        try {
            const jsonValue = JSON.stringify(value)
            await AsyncStorage.setItem('CartProducts', jsonValue)
        } catch (e) {
            alert.log(e)
        }
    }

    useEffect(() => {
        getProductos();
    }, []);

    return (

        <View style={styles.container}>
            <TouchableOpacity style={styles.followButton} onPress={() => navigation.navigate("Cart")}>
                <Text style={styles.followButtonText}>Carrito</Text>
            </TouchableOpacity>

            {isLoading ? <ActivityIndicator /> : (
                <FlatList
                    style={styles.userList}
                    data={data}
                    keyExtractor={({ id }) => id}
                    renderItem={({ item }) => {
                        return (
                            <TouchableOpacity style={styles.card}>
                                <Image style={styles.image} source={require('../assets/producto.png')} />
                                <View style={styles.cardContent}>
                                    <Text style={styles.name} >{item.referencia.nombre}</Text>
                                    <Text style={styles.descripcion}>{item.referencia.descripcion}</Text>
                                    <Text style={styles.price}>$ {item.total}</Text>
                                    <TouchableOpacity style={styles.followButton} onPress={() => this.addProduct(item)}>
                                        <Text style={styles.followButtonText}>Agregar</Text>
                                    </TouchableOpacity>
                                </View>
                            </TouchableOpacity>
                        )
                    }} />
            )}
        </View>
    );
};

const styles = StyleSheet.create({

    image: {
        width: 100,
        height: 100,
    },
    container: {
        flex: 1,
        marginTop: 20,
        backgroundColor: "#eeeeee",
    },
    userList: {
        flex: 1,
    },
    cardContent: {
        marginLeft: 20,
        marginTop: 10
    },
    card: {
        shadowColor: '#00000021',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 12,

        marginVertical: 10,
        marginHorizontal: 20,
        backgroundColor: "white",
        flexBasis: '46%',
        padding: 10,
        flexDirection: 'row',
        borderRadius: 30,
    },
    followButton: {
        marginTop: 10,
        height: 35,
        width: 100,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30,
        backgroundColor: "#00BFFF",
    },
    followButtonText: {
        color: "#FFFFFF",
        fontSize: 20,
    },
    name: {
        fontSize: 17,
        flex: 1,
        color: "#008080",
        fontWeight: 'bold'
    },
    descripcion: {
        paddingRight: 100,
    },
    price: {
        fontSize: 18,
        flex: 1,
        color: "#2b2b2b",
        fontWeight: 'bold'
    }

});





