import React, { useState } from 'react';
import { Button, Text, TextInput, TouchableOpacity, View, StyleSheet, Alert } from 'react-native';
import { useNavigation } from "@react-navigation/native";

const LoginScreen = () => {

    const navigation = useNavigation();
    const [username, setUsername] = useState(null);
    const [password, setPassword] = useState(null);

    const login = (username, password) => {

        fetch('http://78.46.69.39:8007/apis/custom/login/', {
            credentials: 'omit',
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username,
                password
            })
        })
            .then((res) => {
                return res.json();
            })
            .then(res => {
                if (res.key && res.group) {
                    navigation.navigate("ListScreen")
                } else if (res.non_field_errors) {
                    alert("No puede iniciar sesión con las credenciales proporcionadas.")
                } else if (res.username) {
                    alert("El campo username no puede ser nulo");
                } else if (res.password) {
                    alert("El campo password no puede ser nulo");
                }
            })
            .catch(e => {
                alert(e)
            });
    };

    return (
        <View style={styles.container}>
            <View style={styles.wrapper}>

                <Text>Username</Text>
                <TextInput
                    style={styles.input}
                    value={username}
                    placeholder="Enter Username"
                    onChangeText={text => setUsername(text)}
                />
                <Text>Password</Text>

                <TextInput
                    style={styles.input}
                    value={password}
                    placeholder="Enter password"
                    onChangeText={text => setPassword(text)}
                    secureTextEntry
                />

                <Button
                    title="Login"
                    onPress={() => {
                        login(username, password);
                    }}
                />

                <View style={{ flexDirection: 'row', marginTop: 20 }}>
                    <Text>¿No tienes una cuenta? </Text>
                    <TouchableOpacity onPress={() => navigation.navigate('RegisterScreen')}>
                        <Text style={styles.link}>Register</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    wrapper: {
        width: '80%',
    },
    input: {
        marginBottom: 12,
        borderWidth: 1,
        borderColor: '#bbb',
        borderRadius: 5,
        paddingHorizontal: 14,
    },
    link: {
        color: 'blue',
    },
});

export default LoginScreen;
